﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBooster : MonoBehaviour
{
    bool boosted = false;

    private void OnCollisionEnter(Collision other)
    {
        if(!boosted && other.gameObject.tag == "Player")
        {
            KartPrototypeController.instance.EnviSpeedBooster();
            boosted = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        boosted = false;
    }
}
