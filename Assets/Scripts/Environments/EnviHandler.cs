﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviHandler : MonoBehaviour
{
    public static EnviHandler instance;

    [Header("Speed Booster")]
    public float speedBoosterMultiplier;
    public float speedBoosterTime;

    [Header("Obstacle")]
    public float bounceMultiplier;
    public float minBounceForce;
    public float maxBounceForce;

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else instance = this;
    }
}
