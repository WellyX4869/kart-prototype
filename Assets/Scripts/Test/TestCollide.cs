﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCollide : MonoBehaviour
{
    [SerializeField] ThirdPersonMovement character;
    [SerializeField] LayerMask obstacleMask;
    float rightSide = 0f, frontSide = 0f, hypotenuse = 0f;
    List<(Vector3, float)> directions = new List<(Vector3, float)>();

    private void Start()
    {
        rightSide = transform.localScale.x / 2;
        frontSide = transform.localScale.z / 2;
        hypotenuse = Mathf.Sqrt(Mathf.Pow(frontSide, 2) + Mathf.Pow(rightSide, 2));

        // Add Direction and Length / Distance
        directions.Add((transform.forward * frontSide + transform.right * rightSide, hypotenuse));
        directions.Add((transform.forward * frontSide + -transform.right * rightSide, hypotenuse));
        directions.Add((-transform.forward * frontSide + transform.right * rightSide, hypotenuse));
        directions.Add((-transform.forward * frontSide + -transform.right * rightSide, hypotenuse));
        directions.Add((transform.forward * frontSide, frontSide));
        directions.Add((-transform.forward * frontSide, frontSide));
        directions.Add((transform.right * rightSide, rightSide));
        directions.Add((-transform.right * rightSide, rightSide));
    }

    private void Update()
    {
        CheckForObstacle();
    }

    private void CheckForObstacle()
    {
        bool collided = false;

        directions.Clear();
        directions.Add((transform.forward * frontSide + transform.right * rightSide, hypotenuse));
        directions.Add((transform.forward * frontSide + -transform.right * rightSide, hypotenuse));
        directions.Add((-transform.forward * frontSide + transform.right * rightSide, hypotenuse));
        directions.Add((-transform.forward * frontSide + -transform.right * rightSide, hypotenuse));
        directions.Add((transform.forward * frontSide, frontSide));
        directions.Add((-transform.forward * frontSide, frontSide));
        directions.Add((transform.right * rightSide, rightSide));
        directions.Add((-transform.right * rightSide, rightSide));

        for (int i = 0; i < directions.Count; i++)
        {
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(transform.position, directions[i].Item1, out hit, directions[i].Item2, obstacleMask))
            {
                character.OnCollisionEnterObstacle();
                collided = true;
                break;
            }
        }

        if (!collided)
        {
            character.OnCollisionExitObstacle();
        }
    }

    private void OnDrawGizmos()
    {
        List<Vector3> rayDirections = new List<Vector3>();
        rayDirections.Add(transform.forward * frontSide + transform.right * rightSide);
        rayDirections.Add(transform.forward * frontSide + -transform.right * rightSide);
        rayDirections.Add(-transform.forward * frontSide + transform.right * rightSide);
        rayDirections.Add(-transform.forward * frontSide + -transform.right * rightSide);
        rayDirections.Add(transform.forward * frontSide);
        rayDirections.Add(-transform.forward * frontSide);
        rayDirections.Add(transform.right * rightSide);
        rayDirections.Add(-transform.right * rightSide);

        foreach (Vector3 direction in rayDirections)
        {
            Debug.DrawRay(transform.position, direction, Color.green);
        }
    }
}
