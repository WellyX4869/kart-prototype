﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    KartPrototypeController kartController;

    private void Start()
    {
        kartController = KartPrototypeController.instance;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Obstacle"))
        {
            kartController.EnviCollideWithObstacle();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            kartController.EnviExitObstacle();
        }
    }
}
