﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonMovement : MonoBehaviour
{
    [SerializeField] float speed = 6f;
    [SerializeField] float turnSpeed = 15f;
    [SerializeField] float turnSmoothTime = 0.1f;
    
    float turnSmoothVelocity;
    float vertical;
    float horizontal;

    PlayerControls controls;

    private void Awake()
    {
        controls = new PlayerControls();
        controls.Test.MoveVertical.performed += ctx => vertical = ctx.ReadValue<float>();
        controls.Test.MoveVertical.canceled += ctx => vertical = 0f;
        controls.Test.MoveHorizontal.performed += ctx => horizontal = ctx.ReadValue<float>();
        controls.Test.MoveHorizontal.canceled += ctx => horizontal = 0f;
        controls.Test.Enable();
    }

    private void OnDisable()
    {
        controls.Test.Disable();
    }

    bool isStopped = false;
    void Update()
    {
        if (isStopped) { vertical = 0f; horizontal = 0f; }
        int zDirection = vertical > 0 ? 1 : -1;
        zDirection = vertical == 0 ? 0 : zDirection;

        transform.parent.Translate(Vector3.forward * zDirection * speed * Time.deltaTime);
        transform.parent.Rotate(Vector3.up, turnSpeed * horizontal * zDirection * Time.deltaTime);

        //if (direction.magnitude >= 0.1f)
        //{
        //    float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        //    float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
        //    character.transform.rotation = Quaternion.Euler(0f, angle, 0f);
        //    character.Move(direction * speed * zDirection * Time.deltaTime);
        //}
    }

    public void OnCollisionEnterObstacle()
    {
        isStopped = true;
    }

    public void OnCollisionExitObstacle()
    {
        isStopped = false;
    }
}
