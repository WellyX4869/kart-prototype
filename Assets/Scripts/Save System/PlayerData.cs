﻿using System;
using System.IO;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    // Singleton
    public static PlayerData instance;

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;
    }

    [Serializable]
    public class DataMechanic
    {
        public float steerAngle;
        public float timeToMaxSpeed;
        public float timeToMaxBackSpeed;
        public float timeToIdleSpeed;
        public float driftCameraShakiness;
        public float driftAmountWhileIdle;
        public float driftJumpTime;
        public float outsideDriftForce;
        public float driftStopTreshold;
        public float boostMultiplier;
        public float gravity;
        public float roadDrag;
        public float roadMaxSpeed;
        public float roadMaxBackSpeed;
        public float grassDrag;
        public float grassMaxSpeed;
        public float grassMaxBackSpeed;
        public float sandDrag;
        public float sandMaxSpeed;
        public float sandMaxBackSpeed;
    }

    public bool LoadDataMechanic()
    {
        try
        {
            string path = Application.persistentDataPath + "/saveDataMechanic";
            if (File.Exists(path))
            {
                DateTime modification = File.GetLastWriteTime(path);
                DateTime newBuilDateTime = new System.DateTime(2021, 10, 8, 12, 18, 0);
                if (modification < newBuilDateTime)
                {
                    File.Delete(path);
                    return false;
                }
                else
                {
                    string loadJson = File.ReadAllText(path);
                    DataMechanic loadItem = JsonUtility.FromJson<DataMechanic>(loadJson);

                    KartPrototypeController kartController = KartPrototypeController.instance;
                    kartController.SteerAngle           = loadItem.steerAngle;
                    kartController.TimeToMaxSpeed       = loadItem.timeToMaxSpeed;
                    kartController.TimeToMaxBackSpeed   = loadItem.timeToMaxBackSpeed;
                    kartController.TimeToIdleSpeed      = loadItem.timeToIdleSpeed;
                    kartController.DriftCameraShakiness = loadItem.driftCameraShakiness;
                    kartController.DriftAmountWhileIdle = loadItem.driftAmountWhileIdle;
                    kartController.DriftJumpTime        = loadItem.driftJumpTime;
                    kartController.OutsideDriftForce    = loadItem.outsideDriftForce;
                    kartController.DriftStopTreshold    = loadItem.driftStopTreshold;
                    kartController.BoostMultiplier      = loadItem.boostMultiplier;
                    kartController.Gravity              = loadItem.gravity;
                    kartController.RoadDrag             = loadItem.roadDrag;
                    kartController.RoadMaxSpeed         = loadItem.roadMaxSpeed;
                    kartController.RoadMaxBackSpeed     = loadItem.roadMaxBackSpeed;
                    kartController.GrassDrag            = loadItem.grassDrag;
                    kartController.GrassMaxSpeed        = loadItem.grassMaxSpeed;
                    kartController.GrassMaxBackSpeed    = loadItem.grassMaxBackSpeed;
                    kartController.SandDrag             = loadItem.sandDrag;
                    kartController.SandMaxSpeed         = loadItem.sandMaxSpeed;
                    kartController.SandMaxBackSpeed     = loadItem.sandMaxBackSpeed;
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch
        {
            return false;
        }
    }

    public void SaveDataMechanic()
    {
        DataMechanic saveItem = new DataMechanic();

        KartPrototypeController kartController = KartPrototypeController.instance;

        saveItem.steerAngle = kartController.SteerAngle;
        saveItem.timeToMaxSpeed = kartController.TimeToMaxSpeed;
        saveItem.timeToMaxBackSpeed = kartController.TimeToMaxBackSpeed;
        saveItem.timeToIdleSpeed = kartController.TimeToIdleSpeed;
        saveItem.driftCameraShakiness = kartController.DriftCameraShakiness;
        saveItem.driftAmountWhileIdle = kartController.DriftAmountWhileIdle;
        saveItem.driftJumpTime = kartController.DriftJumpTime;
        saveItem.outsideDriftForce = kartController.OutsideDriftForce;
        saveItem.driftStopTreshold = kartController.DriftStopTreshold;
        saveItem.boostMultiplier = kartController.BoostMultiplier;
        saveItem.gravity = kartController.Gravity;
        saveItem.roadDrag = kartController.RoadDrag;
        saveItem.roadMaxSpeed = kartController.RoadMaxSpeed;
        saveItem.roadMaxBackSpeed = kartController.RoadMaxBackSpeed;
        saveItem.grassDrag = kartController.GrassDrag;
        saveItem.grassMaxSpeed = kartController.GrassMaxSpeed;
        saveItem.grassMaxBackSpeed = kartController.GrassMaxBackSpeed;
        saveItem.sandDrag = kartController.SandDrag;
        saveItem.sandMaxSpeed = kartController.SandMaxSpeed;
        saveItem.sandMaxBackSpeed = kartController.SandMaxBackSpeed;

        string path = Application.persistentDataPath + "/saveDataMechanic";
        string saveJson = JsonUtility.ToJson(saveItem, true);
        File.WriteAllText(path, saveJson);
    }
}
