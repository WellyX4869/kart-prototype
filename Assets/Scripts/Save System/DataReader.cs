﻿using UnityEngine;
using System.Reflection;
using System.Collections.Generic;

public class DataReader : MonoBehaviour
{
    public static DataReader instance;

    KartPrototypeController floatDatas;
    SurfaceController boolDatas;

    [HideInInspector] public List<FieldInfo> fields = new List<FieldInfo>();
    public int floatCounts;

    private void Awake()
    {
        instance = this;
    }

    public void SetupDataReader()
    {
        // Setup data reader
        floatDatas = KartPrototypeController.instance;
        boolDatas = SurfaceController.instance;
        List<string> floatParamsNames = floatDatas.publicParams;
        List<(FieldInfo, int)> sortedFloatFields = new List<(FieldInfo, int)>();
        foreach (var field in typeof(KartPrototypeController).GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
        {
            if (floatParamsNames.Contains(field.Name))
            {
                int index = floatParamsNames.IndexOf(field.Name);
                sortedFloatFields.Add((field, index));
            }
        }
        floatCounts = sortedFloatFields.Count;
        List<string> boolParamsNames = boolDatas.publicParams;
        List<(FieldInfo, int)> sortedBoolFields = new List<(FieldInfo, int)>();
        foreach (var field in typeof(SurfaceController).GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
        {
            if (boolParamsNames.Contains(field.Name))
            {
                int index = boolParamsNames.IndexOf(field.Name);
                sortedBoolFields.Add((field, index));
            }
        }

        // Sort data reader
        sortedFloatFields.Sort(delegate ((FieldInfo, int) field1, (FieldInfo, int) field2)
        {
            return field1.Item2.CompareTo(field2.Item2);
        });
        sortedBoolFields.Sort(delegate ((FieldInfo, int) field1, (FieldInfo, int) field2)
        {
            return field1.Item2.CompareTo(field2.Item2);
        });

        // Store the sorted data reader to the fields
        for (int i = 0; i<sortedFloatFields.Count; i++)
        {
            fields.Add(sortedFloatFields[i].Item1);
        }
        for(int i = 0; i < sortedBoolFields.Count; i++)
        {
            fields.Add(sortedBoolFields[i].Item1);
        }
    }

    public float GetFloatValue(int index)
    {
        return (float)fields[index].GetValue(floatDatas);
    }

    public bool GetBoolValue(int index)
    {
        return (bool)fields[index].GetValue(boolDatas);
    }

    public void SetValue(int index, float value)
    {
        if(index < fields.Count)
        {
            fields[index].SetValue(floatDatas, value);
        }
    }

    public void SetValue(int index, bool value)
    {
        if (index < fields.Count)
        {
            fields[index].SetValue(boolDatas, value);
        }
    }
}