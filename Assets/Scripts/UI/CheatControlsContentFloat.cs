﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;

[ExecuteInEditMode]
public class CheatControlsContentFloat : MonoBehaviour
{
    [Header("Serialize Fields")]
    [SerializeField] TextMeshProUGUI parameterNameText;
    public Slider changeValueSlider;
    public InputField changeValueInput;
    public TextMeshProUGUI valueText;
    
    [Header("Contents")]
    public string parameterName;
    public Vector2 rangeSlider;
    public bool isWholeNumber;
    public float value;

    int index = -1;

    private void Start()
    {
        index = transform.GetSiblingIndex();
        if(index %2 == 0)
            GetComponent<Image>().color = CheatControls.instance.oddContentColor;
        else GetComponent<Image>().color = CheatControls.instance.evenContentColor;
    }

    private void Update()
    {
        if (Application.isPlaying) { return; }

        ChangeValue(value);
    }

    public void ChangeValue(float newValue)
    {
        gameObject.name = parameterName;
        parameterNameText.text = parameterName;
        changeValueSlider.minValue = rangeSlider.x; changeValueSlider.maxValue = rangeSlider.y;
        changeValueSlider.wholeNumbers = isWholeNumber;
        value = Mathf.Clamp(newValue, rangeSlider.x, rangeSlider.y);
        value = isWholeNumber ? Mathf.Round(value) : Mathf.Round(value * 100f) / 100f;
        changeValueSlider.value = value;
        changeValueInput.text = value.ToString();
        valueText.text = value.ToString();
    }

    public void SetParamTextSize()
    {
        int maxNormalLength = 20;

        if (parameterName.Length > maxNormalLength)
        {
            int modLength = parameterName.Length - maxNormalLength;
            float textSize = parameterNameText.fontSize;
            textSize = textSize - (2 * (modLength / 2));
            parameterNameText.fontSize = textSize;
        }
    }

    public void ChangeSliderValue()
    {
        float newValue = changeValueSlider.value;
        value = Mathf.Clamp(newValue, rangeSlider.x, rangeSlider.y);
        value = isWholeNumber ? Mathf.Round(value) : Mathf.Round(value * 100f) / 100f;
        changeValueInput.text = value.ToString();
        valueText.text = value.ToString();

        if(index != -1)
            CheatControls.instance.ChangeGameplayValue(index, value);
    }

    public void ChangeInputValue()
    {
        float newValue = float.Parse(changeValueInput.text.ToString());
        value = Mathf.Clamp(newValue, rangeSlider.x, rangeSlider.y);
        value = isWholeNumber ? Mathf.Round(value) : Mathf.Round(value * 100f) / 100f;
        changeValueSlider.value = value;
        valueText.text = value.ToString();

        if(index != -1)
            CheatControls.instance.ChangeGameplayValue(index, value);
    }
}
