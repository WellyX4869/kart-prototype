﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CheatControls : MonoBehaviour
{
    public static CheatControls instance;

    public Transform cheatControlParent;
    public GameObject cheatControlFloatPrefab;
    public GameObject cheatControlBoolPrefab;
    public Scrollbar scrollBar;
    public Color oddContentColor;
    public Color evenContentColor;

    // Important List 
    List<CheatControlsContentFloat> cheatControlContentsFloat = new List<CheatControlsContentFloat>();
    List<CheatControlsContentBool> cheatControlContentsBool = new List<CheatControlsContentBool>();
    List<float> defaultFloatValues = new List<float>();
    List<bool> defaultBoolValues = new List<bool>();
    DataReader dataReader;

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;
    }

    private void Start()
    {
        PlayerData.instance.LoadDataMechanic();
        defaultFloatValues.Clear();
        defaultBoolValues.Clear();

        dataReader = DataReader.instance;
        dataReader.SetupDataReader();
        SetupCheatControlContent();
    }

    private void SetupCheatControlContent()
    {
        // Float Values
        KartPrototypeController kartController = KartPrototypeController.instance;
        for(int i = 0; i< dataReader.floatCounts; i++)
        {
            AddCheatControlContents(dataReader.fields[i].Name, dataReader.GetFloatValue(i), kartController.publicConfigParams[i]);
        }

        // Bool Values
        for (int i = dataReader.floatCounts; i< dataReader.fields.Count; i++)
        {
            AddCheatControlContents(dataReader.fields[i].Name, dataReader.GetBoolValue(i));
        }
    }

    void AddCheatControlContents(string paramName, float value, Vector3 paramConfig)
    {
        CheatControlsContentFloat cheatItem = Instantiate(cheatControlFloatPrefab, cheatControlParent).GetComponent<CheatControlsContentFloat>();
        cheatItem.parameterName = GetProperParamName(paramName);
        cheatItem.SetParamTextSize();
        cheatItem.rangeSlider = new Vector2(paramConfig.x, paramConfig.y);
        cheatItem.isWholeNumber = paramConfig.z == 1? true : false;
        cheatItem.ChangeValue(value);
        cheatControlContentsFloat.Add(cheatItem);
        defaultFloatValues.Add(value);
    }

    void AddCheatControlContents(string paramName, bool value)
    {
        CheatControlsContentBool cheatItem = Instantiate(cheatControlBoolPrefab, cheatControlParent).GetComponent<CheatControlsContentBool>();
        cheatItem.parameterName = GetProperParamName(paramName);
        cheatItem.SetParamTextSize();
        cheatItem.ChangeValue(value);
        cheatControlContentsBool.Add(cheatItem);
        defaultBoolValues.Add(value);
    }

    string GetProperParamName(string paramName)
    {
        string name = "";
        for(int i = 0; i < paramName.Length; i++)
        {
            if(i == 0)
                name += paramName[i].ToString().ToUpper();
            else
                name += paramName[i];
    
            if(i < paramName.Length - 2 && Char.IsUpper(paramName[i+1]))
            {
                name += " ";
            }
        }
        return name;
    }

    public void ResetToDefaults()
    {
        for (int i = 0; i < defaultFloatValues.Count; i++)
        {
            cheatControlContentsFloat[i].ChangeValue(defaultFloatValues[i]);
        }
        for (int i = 0; i < defaultBoolValues.Count; i++)
        {
            cheatControlContentsBool[i].ChangeValue(defaultBoolValues[i]);
        }
    }

    public void ChangeGameplayValue(int index, float value)
    {
        dataReader.SetValue(index, value);
    }

    public void ChangeGameplayValue(int index, bool value)
    {
        int cheatIndex = index - dataReader.floatCounts;
        dataReader.SetValue(index, value);
        if (value)
        {
            for(int i = 0; i<dataReader.fields.Count - dataReader.floatCounts; i++)
            {
                if(i == cheatIndex) { continue; }
                cheatControlContentsBool[i].ChangeValue(false);
            }
        }
        SurfaceController.instance.ChangeSurfaceFrictions();
    }

    public void SaveSettings()
    {
        PlayerData.instance.SaveDataMechanic();

        // Float Values
        for(int i = 0; i<defaultFloatValues.Count; i++)
        {
            defaultFloatValues[i] = dataReader.GetFloatValue(i);
        }
    }
}
