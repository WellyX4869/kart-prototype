﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;

[ExecuteInEditMode]
public class CheatControlsContentBool : MonoBehaviour
{
    [Header("Serialize Fields")]
    [SerializeField] TextMeshProUGUI parameterNameText;
    public Slider changeValueSlider;
    public Toggle changeValueToggle;
    public TextMeshProUGUI valueText;

    [Header("Contents")]
    public string parameterName;
    public Vector2 rangeSlider;
    bool isWholeNumber;
    public bool value;

    int index = -1;
    private bool isAlreadySetup = false;

    private void Start()
    {
        index = transform.GetSiblingIndex();
        if (index % 2 == 0)
            GetComponent<Image>().color = CheatControls.instance.oddContentColor;
        else GetComponent<Image>().color = CheatControls.instance.evenContentColor;
        isAlreadySetup = false;
    }

    private void Update()
    {
        if (Application.isPlaying) { return; }

        ChangeValue(value);
    }

    public void ChangeValue(bool newValue)
    {
        if (!isAlreadySetup)
        {
            gameObject.name = parameterName;
            parameterNameText.text = parameterName;
            rangeSlider = new Vector2(0, 1);
            isWholeNumber = true;
            changeValueSlider.minValue = rangeSlider.x; changeValueSlider.maxValue = rangeSlider.y;
            changeValueSlider.wholeNumbers = isWholeNumber;
            isAlreadySetup = true;
        }
        changeValueSlider.value = newValue == true? 1f : 0f;
        changeValueToggle.isOn = newValue;
        value = newValue;
        valueText.text = value.ToString();
    }

    public void SetParamTextSize()
    {
        int maxNormalLength = 20;
        if(parameterName.Length > maxNormalLength)
        {
            int modLength = parameterName.Length - maxNormalLength;
            float textSize = parameterNameText.fontSize;
            textSize = textSize - (2 * (modLength / 2));
            parameterNameText.fontSize = textSize;
        }
    }

    public void ChangeSliderValue()
    {
        float newValue = changeValueSlider.value;
        value = newValue == 1 ? true : false;
        changeValueToggle.isOn = value;
        valueText.text = value.ToString();
        
        if(index != -1)
            CheatControls.instance.ChangeGameplayValue(index, value);
    }

    public void ChangeToggleValue()
    {
        value = changeValueToggle.isOn;
        changeValueSlider.value = value?1:0;
        valueText.text = value.ToString();

        if (index != -1)
            CheatControls.instance.ChangeGameplayValue(index, value);
    }
}
