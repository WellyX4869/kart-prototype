﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [SerializeField] GameObject settingsHintText;
    [SerializeField] GameObject controlsUI;
    [SerializeField] GameObject cheatSettingsUI;
    [SerializeField] GameObject displaySettingsUI;
    [SerializeField] GameObject settingsUI;

    [Header("Settings UI")]
    [SerializeField] Button[] settingButtons;

    [Header("Display Settings UI")]
    [SerializeField] Button firstDisplaySettingButton;
    [SerializeField] Toggle fullscreenToggle;

    PlayerControls controls;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        controls = new PlayerControls();
        controls.Settings.Back.performed += ctx => UIManager.instance.DisplaySettings(false);
    }

    private void Start()
    {
        fullscreenToggle.onValueChanged.AddListener(SetFullScreen);

        controlsUI.SetActive(false);
        cheatSettingsUI.SetActive(false);
        settingsUI.SetActive(false);
        settingsHintText.SetActive(true);
    }

    private void LateUpdate()
    {
        if (fullscreenToggle.isOn != Screen.fullScreen)
        {
            fullscreenToggle.SetIsOnWithoutNotify(Screen.fullScreen);
        }
    }

    public void DisplaySettings(bool isDisplayed)
    {
        if (isDisplayed)
        {
            settingsHintText.SetActive(false);
            controls.Settings.Enable();
            KartPrototypeController.instance.controls.Gameplay.Disable();
            settingsUI.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(settingButtons[0].gameObject);
            //Time.timeScale = 0f;
        }
        else
        {
            if(controlsUI.activeSelf == true)
            {
                controlsUI.SetActive(false);
                settingsUI.SetActive(true);
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(settingButtons[0].gameObject);
            }
            else if (displaySettingsUI.activeSelf == true)
            {
                displaySettingsUI.SetActive(false);
                settingsUI.SetActive(true);
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(settingButtons[1].gameObject);
            }
            else if(cheatSettingsUI.activeSelf == true)
            {
                cheatSettingsUI.SetActive(false);
                settingsUI.SetActive(true);
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(settingButtons[2].gameObject);
            }
            else
            {
                settingsUI.SetActive(false);
                controls.Settings.Disable();
                KartPrototypeController.instance.controls.Gameplay.Enable();
                settingsHintText.SetActive(true);
            }
        }
    }

    public void DisplayControls()
    {
        settingsUI.SetActive(false);
        controlsUI.SetActive(true);
    }

    public void ShowDisplaySettings()
    {
        settingsUI.SetActive(false);
        displaySettingsUI.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstDisplaySettingButton.gameObject);
    }

    public void DisplayCheatSettings()
    {
        settingsUI.SetActive(false);
        cheatSettingsUI.SetActive(true);
    }

    public void SetResolution16x9(int width)
    {
        Screen.SetResolution(width, Mathf.RoundToInt(width * (9 / 16f)), Screen.fullScreenMode);
    }

    public void SetFullScreen(bool fullscreenOn)
    {
        //Screen.fullScreen = fullscreenOn;
        var nativeRes = Screen.resolutions[Screen.resolutions.Length - 1];
        if (fullscreenOn)
        {
            Screen.SetResolution(nativeRes.width, nativeRes.height, FullScreenMode.FullScreenWindow);
        }
        else
        {
            float windowedScale = 0.75f;
            int x = nativeRes.width / 16;
            int y = nativeRes.height / 9;
            int m = (int)(Mathf.Min(x, y) * windowedScale);
            Screen.SetResolution(16 * m, 9 * m, FullScreenMode.Windowed);
        }

    }
}
