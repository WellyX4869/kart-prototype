﻿using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{
    public static CameraFollowPlayer instance;
    [SerializeField] Vector3 offset;
    [SerializeField] Transform player;
    [SerializeField] Vector3 origCamPos;
    [SerializeField] Vector3 boostCamPos;
    [SerializeField] Camera[] cameras;

    public bool isCameraStill = false;
    public bool isRearCameraActive = false;
    private float camPosY;

    [Header("Shake Camera Config")]
    public float duration = 0.3f;
    public Vector3 shakeStrength;

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        var playerPos = player.position;
        if (isCameraStill)
        {
            playerPos.y = camPosY;
            transform.position = playerPos + offset;
        }
        else
        {
            playerPos.y = camPosY;
            transform.position = Vector3.Lerp(playerPos + offset, player.position + offset, 40 * Time.deltaTime);
            camPosY = player.position.y;
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, player.eulerAngles.y, 0), 3 * Time.deltaTime); //we only want the camera rotate on the y axis. Not the x or z axis since when the player is turning in the air while gliding, the player is tilting a lot, so we do not want the camera to tilt sideways too.
        transform.GetChild(0).localPosition = Vector3.Lerp(transform.GetChild(0).localPosition, origCamPos, 3 * Time.deltaTime);
    }

    public void ChangeCamera()
    {
        // Get the active camera
        int activeCameraIndex = 0;
        for(int i = 0; i<cameras.Length; i++)
        {
            if(cameras[i].depth >= 0)
            {
                activeCameraIndex = i;
                break;
            }
        }

        isRearCameraActive = false;
        if (activeCameraIndex == 0)
            isRearCameraActive = true;
        cameras[activeCameraIndex].depth = -1;
        cameras[(activeCameraIndex + 1) % cameras.Length].depth = 1;
    }
}
