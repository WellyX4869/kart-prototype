﻿using UnityEngine;
using DG.Tweening;
using TMPro;
using System.Collections.Generic;
using System;

public class KartPrototypeController : MonoBehaviour
{
    public static KartPrototypeController instance;
    public Transform kartModel;
    public Transform kartNormal;
    public Rigidbody sphereRigidBody;

    float speed, currentSpeed, realSpeed;
    float rotate, currentRotate;
    int driftDirection;
    public float RealSpeed { get => realSpeed; }

    float driftPower;
    int driftMode = 0, steerMode = 0;
    bool first, second, third;
    float boostMultiplier = 3f;

    public LayerMask groundMask;
    public bool drifting;

    [Header("Parameters")]
    [SerializeField] float steerAngle = 80f;
    [SerializeField] float gravity = 10f;
    [SerializeField] float driftStopTreshold = 30f;
    [SerializeField] float outsideDriftForce = 50000f;
    [SerializeField] float driftJumpForce = 2000f;
    [SerializeField] float driftCameraShakiness = 0.1f;
    [SerializeField] float driftAmountWhileIdle = 7f;
    [SerializeField] float timeToMaxSpeed = 0.5f;
    [SerializeField] float timeToMaxBackSpeed = 1f;
    [SerializeField] float timeToIdle = 1.5f;
    [SerializeField] float roadDrag = 2.5f;
    [SerializeField] float roadMaxSpeed = 60f;
    [SerializeField] float roadMaxBackSpeed = 10f;
    [SerializeField] float grassDrag = 2.5f;
    [SerializeField] float grassMaxSpeed = 45f;
    [SerializeField] float grassMaxBackSpeed = 35f;
    [SerializeField] float sandDrag = 3.5f;
    [SerializeField] float sandMaxSpeed = 45f;
    [SerializeField] float sandMaxBackSpeed = 35f;

    [HideInInspector] [SerializeField] [Range(0.15f, 0.4f)] float driftDelayTime = 0.3f;

    #region PUBLIC_PARAMS_FOR_TESTING_PURPOSES
    public float SteerAngle { get => steerAngle; set => steerAngle = value; } 
    public float TimeToMaxSpeed { get => timeToMaxSpeed; set => timeToMaxSpeed = value; }
    public float TimeToMaxBackSpeed { get => timeToMaxBackSpeed; set => timeToMaxBackSpeed = value; }
    public float TimeToIdleSpeed { get => timeToIdle; set => timeToIdle = value; }
    public float DriftCameraShakiness { get => driftCameraShakiness; set => driftCameraShakiness = value; }
    public float DriftAmountWhileIdle { get => driftAmountWhileIdle; set => driftAmountWhileIdle = value; }
    public float DriftJumpTime { get => driftDelayTime; set => driftDelayTime = value; }
    public float OutsideDriftForce { get => outsideDriftForce; set => outsideDriftForce = value; }
    public float DriftStopTreshold { get => driftStopTreshold; set => driftStopTreshold = value; }
    public float BoostMultiplier { get => boostMultiplier; set => boostMultiplier = value; }
    public float Gravity { get => gravity; set => gravity = value; }
    public float RoadDrag { get => roadDrag; set => roadDrag = value; }
    public float RoadMaxSpeed { get => roadMaxSpeed; set => roadMaxSpeed = value; }
    public float RoadMaxBackSpeed { get => roadMaxBackSpeed; set => roadMaxBackSpeed = value; }
    public float GrassDrag { get => grassDrag; set => grassDrag = value; }
    public float GrassMaxSpeed { get => grassMaxSpeed; set => grassMaxSpeed = value; }
    public float GrassMaxBackSpeed { get => grassMaxBackSpeed; set => grassMaxBackSpeed = value; }
    public float SandDrag { get => sandDrag; set => sandDrag = value; }
    public float SandMaxSpeed { get => sandMaxSpeed; set => sandMaxSpeed = value; }
    public float SandMaxBackSpeed { get => sandMaxBackSpeed; set => sandMaxBackSpeed = value; }

    [Header("Config (x = min, y = max, z = wholeNumber)")]
    public Vector3 SteerAngleConfig;
    public Vector3 TimeToMaxSpeedConfig;
    public Vector3 TimeToMaxBackSpeedConfig;
    public Vector3 TimeToIdleSpeedConfig;
    public Vector3 DriftCameraShakinessConfig;
    public Vector3 DriftAmountWhileIdleConfig;
    public Vector3 DriftJumpTimeConfig;
    public Vector3 OutsideDriftForceConfig;
    public Vector3 DriftStopTresholdConfig;
    public Vector3 BoostMultiplierConfig;
    public Vector3 GravityConfig;
    public Vector3 RoadDragConfig;
    public Vector3 RoadMaxSpeedConfig;
    public Vector3 RoadMaxBackSpeedConfig;
    public Vector3 GrassDragConfig;
    public Vector3 GrassMaxSpeedConfig;
    public Vector3 GrassMaxBackSpeedConfig;
    public Vector3 SandDragConfig;
    public Vector3 SandMaxSpeedConfig;
    public Vector3 SandMaxBackSpeedConfig;
    #endregion;

    [Header("Model Parts")]
    [SerializeField] Transform steeringWheel;
    [SerializeField] Transform frontWheels;
    [SerializeField] Transform backWheels;

    [Header("Particles, etc")]
    //[SerializeField] TrailRenderer[] tireSkidMarks;
    [SerializeField] GameObject carStatusCanvas;
    [SerializeField] TextMeshProUGUI carStatus;
    [SerializeField] bool displayCarStatus;
    [SerializeField] Color[] turboColors;
    [SerializeField] ParticleSystem[] boostParticles;

    [HideInInspector] public PlayerControls controls;
    CameraFollowPlayer camInstance = null;
    EnviHandler enviHandler = null;
    float moveHorizontal;
    bool gasOn;
    bool retreatOn;
    bool isGrounded = true;
    bool isOffroad = false;
    float waitForDriftingAgain = 0f, waitForSteerDrifting = 0f;
    bool canSteerWhenIdleDrifting;
    int lastSteerDirection = 0;
    float dragAmount = 0f;
    float speedLerpTime = 0f;
    float timeForUnfreezingRotation = 0f;
    float timeStampSinceStart = 0f;
    bool kartIsIdle = true;
    bool kartFromRetreatToGas = false;
    bool kartFromGasToRetreat = false;
    bool isStillJumpDrifting = false;
    bool isStuckAtObstacle = false;
    [HideInInspector] public List<string> publicParams = new List<string>();
    [HideInInspector] public List<Vector3> publicConfigParams = new List<Vector3>();

    /// <summary>
    /// Gamepad Controls
    /// </summary>
    private void Awake()
    {
        // Set singleton
        instance = this;

        GetListPublicParams();
        displayCarStatus = true;
        controls = new PlayerControls();
        controls.Gameplay.Gas.performed += ctx => gasOn = true;
        controls.Gameplay.Gas.canceled += ctx => gasOn = false;
        controls.Gameplay.Retreat.performed += ctx => retreatOn = true;
        controls.Gameplay.Retreat.canceled += ctx => retreatOn = false;

        controls.Gameplay.Drift.performed += ctx => Drift();
        controls.Gameplay.Drift.canceled += ctx => Boost();
        controls.Gameplay.Move.performed += ctx => moveHorizontal = ctx.ReadValue<float>();
        controls.Gameplay.Move.canceled += ctx => moveHorizontal = 0f;

        controls.Gameplay.CarStatus.performed += ctx => DisplayCarStatus();
        controls.Gameplay.Settings.performed += ctx => UIManager.instance.DisplaySettings(true);

        controls.Gameplay.ChangeCam.performed += ctx => CameraFollowPlayer.instance.ChangeCamera();
    }

    public void GetListPublicParams()
    {
        publicParams.Add(nameof(steerAngle));
        publicParams.Add(nameof(timeToMaxSpeed));
        publicParams.Add(nameof(timeToMaxBackSpeed));
        publicParams.Add(nameof(timeToIdle));
        publicParams.Add(nameof(driftCameraShakiness));
        publicParams.Add(nameof(driftAmountWhileIdle));
        publicParams.Add(nameof(driftDelayTime));
        publicParams.Add(nameof(outsideDriftForce));
        publicParams.Add(nameof(driftStopTreshold));
        publicParams.Add(nameof(boostMultiplier));
        publicParams.Add(nameof(gravity));
        publicParams.Add(nameof(roadDrag));
        publicParams.Add(nameof(roadMaxSpeed));
        publicParams.Add(nameof(roadMaxBackSpeed));
        publicParams.Add(nameof(grassDrag));
        publicParams.Add(nameof(grassMaxSpeed));
        publicParams.Add(nameof(grassMaxBackSpeed));
        publicParams.Add(nameof(sandDrag));
        publicParams.Add(nameof(sandMaxSpeed));
        publicParams.Add(nameof(sandMaxBackSpeed));

        publicConfigParams.Add(SteerAngleConfig);
        publicConfigParams.Add(TimeToMaxSpeedConfig);
        publicConfigParams.Add(TimeToMaxBackSpeedConfig);
        publicConfigParams.Add(TimeToIdleSpeedConfig);
        publicConfigParams.Add(DriftCameraShakinessConfig);
        publicConfigParams.Add(DriftAmountWhileIdleConfig);
        publicConfigParams.Add(DriftJumpTimeConfig);
        publicConfigParams.Add(OutsideDriftForceConfig);
        publicConfigParams.Add(DriftStopTresholdConfig);
        publicConfigParams.Add(BoostMultiplierConfig);
        publicConfigParams.Add(GravityConfig);
        publicConfigParams.Add(RoadDragConfig);
        publicConfigParams.Add(RoadMaxSpeedConfig);
        publicConfigParams.Add(RoadMaxBackSpeedConfig);
        publicConfigParams.Add(GrassDragConfig);
        publicConfigParams.Add(GrassMaxSpeedConfig);
        publicConfigParams.Add(GrassMaxBackSpeedConfig);
        publicConfigParams.Add(SandDragConfig);
        publicConfigParams.Add(SandMaxSpeedConfig);
        publicConfigParams.Add(SandMaxBackSpeedConfig);
    }

    private void Start()
    {
        sphereRigidBody.velocity = Vector3.zero;
        camInstance = CameraFollowPlayer.instance;
        enviHandler = EnviHandler.instance;
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }

    public float offsetY = 0.025f;
    public float offsetZ = 0.05f;
    public float distanceToGround = 0.5f;

    void Update()
    {
        //Follow Collider
        transform.position = sphereRigidBody.transform.position + new Vector3(0f, offsetY, offsetZ);
        waitForDriftingAgain = waitForDriftingAgain > 10f ? 3f : waitForDriftingAgain;
        waitForDriftingAgain += Time.deltaTime;

        // Don't handle input unless it is time
        if (timeStampSinceStart < 0.5f)
        {
            timeStampSinceStart += Time.deltaTime;
        }
        else
        {
            realSpeed = transform.InverseTransformDirection(sphereRigidBody.velocity).z;

            string carStatusText = "";
            // Accelerate / Move
            if (gasOn)
            {
                speed = roadMaxSpeed;
                carStatusText = "Driving";
                speedLerpTime = timeToMaxSpeed;
                if(!kartFromRetreatToGas || kartIsIdle)
                {
                    kartFromRetreatToGas = true;
                    timeForUnfreezingRotation = 0f;
                }
                kartFromGasToRetreat = false;
                kartIsIdle = false;
            }
            else if (retreatOn)
            {
                speed = -roadMaxBackSpeed;
                carStatusText = "Retreat";
                speedLerpTime = timeToMaxBackSpeed;
                if (!kartFromGasToRetreat || kartIsIdle)
                {
                    kartFromGasToRetreat = true;
                    timeForUnfreezingRotation = 0f;
                }
                kartIsIdle = false;
                kartFromRetreatToGas = false;
            }
            else
            {
                if (!kartIsIdle)
                {
                    speedLerpTime = timeToIdle * (Mathf.Abs(currentSpeed)-0) / 20f;
                    speedLerpTime = Mathf.Clamp(speedLerpTime, timeToIdle / 5f, speedLerpTime/1.5f);
                    kartIsIdle = true;
                    kartFromGasToRetreat = false;
                    kartFromRetreatToGas = false;
                }
            }

            //Change freeze rotation enabled or not to make car easier to move from idle and stop precisely without sliding
            if (timeForUnfreezingRotation < timeToIdle && kartIsIdle)
            {
                sphereRigidBody.freezeRotation = false;
            }
            else if (timeForUnfreezingRotation < 2.5f && (kartFromGasToRetreat || kartFromRetreatToGas))
            {
                sphereRigidBody.freezeRotation = false;
            }
            else
            {
                sphereRigidBody.freezeRotation = true;
            }

            timeForUnfreezingRotation += Time.deltaTime;

            // Ground Check
            GroundCheck();

            // Steer
            float steerAmount = 0f;
            int direction = 0;
            if (moveHorizontal != 0f)
            {
                direction = moveHorizontal > 0 ? 1 : -1;
                steerAmount = Mathf.Abs((moveHorizontal));
                steerAmount = Mathf.Abs(realSpeed) > driftStopTreshold ? steerAmount * 0.8f : Mathf.Abs(realSpeed) / driftStopTreshold * steerAmount;
                Steer(direction, steerAmount);
            }

            // Drifting Checking If it still jump
            if (isStillJumpDrifting)
            {
                // Apply Shakiness to camera if it is met right condition
                if (Mathf.Abs(realSpeed) < driftStopTreshold/2f && waitForDriftingAgain >= 0.9f * driftDelayTime)
                {
                    Camera.main.transform.DOPunchPosition(camInstance.shakeStrength * driftCameraShakiness, camInstance.duration, 20, 1);
                }

                if (waitForDriftingAgain >= driftDelayTime)
                {
                    camInstance.isCameraStill = false;
                    isStillJumpDrifting = false;
                }
            }

            // Drifting Checking If Can Idle Drifting
            if (canSteerWhenIdleDrifting)
            {
                if (moveHorizontal != 0f)
                {
                    driftDirection = moveHorizontal > 0 ? 1 : -1;
                    Steer(driftDirection, driftAmountWhileIdle);
                }
                canSteerWhenIdleDrifting = false;
            }

            // Drifting Mechanic
            if (drifting)
            {
                float control = (driftDirection == 1) ? ExtensionMethods.Remap(moveHorizontal, -1, 1, 0.05f, 1f) : ExtensionMethods.Remap(moveHorizontal, -1, 1, 1f, 0.05f);
                float powerControl = (driftDirection == 1) ? ExtensionMethods.Remap(moveHorizontal, -1, 1, .2f, 1) : ExtensionMethods.Remap(moveHorizontal, -1, 1, 1, .2f);
                float driftControlSpeed = Mathf.Abs(realSpeed) > driftStopTreshold ? control : 0f;
                driftPower += powerControl * 45f * Time.deltaTime;

                // If the real speed is not enough to maintain drifting
                if (driftControlSpeed <= 0f)
                {
                    Boost();
                }
                else
                {
                    Steer(driftDirection, control);
                }

                string leftOrRight = driftDirection == 1 ? "right" : "left";
                carStatusText += "\nDrifting to " + leftOrRight;

                ColorDrift();
            }
            else // SteerDrifting Mechanic
            {
                if (isOffroad)
                {
                    Boost(false);
                    lastSteerDirection = 0;
                    waitForSteerDrifting = 0f;
                    if (moveHorizontal != 0f)
                    {
                        string leftOrRight = direction == 1 ? "right" : "left";
                        carStatusText += "\nSteer to " + leftOrRight;
                    }
                }
                else
                {
                    if (moveHorizontal != 0f)
                    {
                        string leftOrRight = direction == 1 ? "right" : "left";
                        carStatusText += "\nSteer to " + leftOrRight;

                        if (realSpeed > driftStopTreshold)
                        {
                            if (lastSteerDirection != direction)
                            {
                                waitForSteerDrifting = 0f;
                                lastSteerDirection = direction;
                                if (steerMode > 0)
                                {
                                    Boost(false);
                                }
                            }
                            waitForSteerDrifting += Time.deltaTime;
                        }
                        else
                        {
                            steerMode = 0;
                            waitForSteerDrifting = 0;
                        }
                    }
                    else
                    {
                        if (steerMode > 0)
                        {
                            Boost(false);
                        }
                        waitForSteerDrifting = 0f;
                        lastSteerDirection = 0;
                    }
                }
                ColorSteer();
            }

            carStatus.text = carStatusText;
            float rotateY = CameraFollowPlayer.instance.isRearCameraActive ? 180 : 0;
            carStatus.transform.localEulerAngles = new Vector3(0, rotateY, 0);
            PlayerLog();

            currentSpeed = Mathf.Lerp(currentSpeed, speed, Time.deltaTime * 1f / speedLerpTime); speed = 0f;
            currentRotate = Mathf.Lerp(currentRotate, rotate, Time.deltaTime * 4f); rotate = 0f;

            #region Animations            
            CheckDriftingForSkidMarks();

            //a) Kart
            if (!drifting)
            {
                kartModel.localEulerAngles = Vector3.Lerp(kartModel.localEulerAngles, new Vector3(0, 90 + (steerAmount * direction * 5f), kartModel.localEulerAngles.z), .2f);            }
            else
            {
                float control = (driftDirection == 1) ? ExtensionMethods.Remap(moveHorizontal, -1, 1, .5f, 1.2f) : ExtensionMethods.Remap(moveHorizontal, -1, 1, 1.2f, .5f);
                kartModel.parent.localRotation = Quaternion.Euler(0, Mathf.LerpAngle(kartModel.parent.localEulerAngles.y, (control * 10) * driftDirection, .2f), 0);
                if (isOffroad) Boost();
            }

            //b) Wheels
            int forwardDirection = realSpeed >= 0f ? 1 : -1;
            foreach (Transform frontWheel in frontWheels.GetComponentInChildren<Transform>())
            {
                frontWheel.localEulerAngles = new Vector3(0, (moveHorizontal * 10f), frontWheel.localEulerAngles.z);
                frontWheel.localEulerAngles += new Vector3(0, 0, sphereRigidBody.velocity.magnitude / 1f * forwardDirection);
            }
            foreach (Transform backWheel in backWheels.GetComponentInChildren<Transform>())
            {
                backWheel.localEulerAngles += new Vector3(0, 0, sphereRigidBody.velocity.magnitude / 1f * forwardDirection);
            }

            ////c) Steering Wheel
            //steeringWheel.localEulerAngles = new Vector3(-25, 90, ((moveHorizontal * 45)));
            #endregion
        }
    }

    private void FixedUpdate()
    {
        sphereRigidBody.drag = dragAmount;
        
        float newCurrentSpeed = currentSpeed;
        if (currentSpeed > 0)
        {
            newCurrentSpeed = isStuckAtObstacle ? 0f : currentSpeed;
        }
        
        //Forward Acceleration
        if (!drifting)
        {
            sphereRigidBody.AddForce(-kartModel.transform.right * newCurrentSpeed, ForceMode.Acceleration);
        }
        else
        {
            if (isGrounded)
            {
                Vector3 force = driftDirection > 0 ? -transform.right * outsideDriftForce * Time.fixedDeltaTime : transform.right * outsideDriftForce * Time.deltaTime;
                sphereRigidBody.AddForce(force, ForceMode.Acceleration);
            }
            sphereRigidBody.AddForce(transform.forward * newCurrentSpeed, ForceMode.Acceleration);
        }

        //Gravity
        sphereRigidBody.AddForce(Vector3.down * gravity, ForceMode.Acceleration);

        //Steering
        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(0, transform.eulerAngles.y + currentRotate, 0), Time.fixedDeltaTime * 5f);

        //Normal Rotation
        RaycastHit hitNear;
        Physics.Raycast(sphereRigidBody.transform.position, Vector3.down, out hitNear, distanceToGround, groundMask);

        kartNormal.up = Vector3.Lerp(kartNormal.up, hitNear.normal, Time.deltaTime * 1f);
        kartNormal.Rotate(0, transform.eulerAngles.y, 0);
    }

    private void PlayerLog()
    {
        //Debug.Log("Speed: " + speed);
        //Debug.Log("CurrentSpeed: " + currentSpeed);
        //if(isOffroad) Debug.Log("Offroad: " + isOffroad);
    }

    float lastSpeed = 0f;
    private void GroundCheck()
    {
        RaycastHit hit;

        if (Physics.Raycast(sphereRigidBody.transform.position, Vector3.down, out hit, distanceToGround, groundMask))
        {
            isGrounded = true;
            if (hit.transform.CompareTag("Road") || hit.transform.CompareTag("Ground"))
            {
                isOffroad = false;
                dragAmount = roadDrag;
                lastSpeed = speed;
            }
            else if (hit.transform.CompareTag("Sand"))
            {
                isOffroad = true;
                dragAmount = sandDrag;
                if (speed == roadMaxSpeed)
                {
                    speed = sandMaxSpeed;
                    currentSpeed = currentSpeed > sandMaxSpeed ? sandMaxSpeed : currentSpeed;
                }
                else if (speed == -roadMaxBackSpeed)
                {
                    speed = -sandMaxBackSpeed;
                    currentSpeed = currentSpeed < -sandMaxBackSpeed ? -sandMaxBackSpeed : currentSpeed;
                }
                lastSpeed = speed;
            }
            else if (hit.transform.CompareTag("Grass"))
            {
                isOffroad = true;
                dragAmount = grassDrag;
                if (speed == roadMaxSpeed)
                {
                    speed = grassMaxSpeed;
                    currentSpeed = currentSpeed > grassMaxSpeed ? grassMaxSpeed : currentSpeed;
                }
                else if (speed == -roadMaxBackSpeed)
                {
                    speed = -grassMaxBackSpeed;
                    currentSpeed = currentSpeed < -grassMaxBackSpeed ? -grassMaxBackSpeed : currentSpeed;
                }
                lastSpeed = speed;
            }
            else
            {
                if (isStillJumpDrifting && speed != 0)
                    speed = lastSpeed;
            }
        }
        else
        {
            if (isStillJumpDrifting && speed != 0)
                speed = lastSpeed;
            isGrounded = false;
            isOffroad = false;
        }
    }

    private void Speed(float x)
    {
        currentSpeed = x;
    }

    #region Player Control Functions
    private void Steer(int direction, float amount)
    {
        int zDirection = currentSpeed >= 0 ? 1 : -1;
        zDirection = Mathf.Abs(currentSpeed) <= 0.05f ? 1 : zDirection;
        rotate = (steerAngle * direction * zDirection) * amount;
    }

    private void Drift()
    {
        if (!drifting && isGrounded && waitForDriftingAgain >= driftDelayTime)
        {
            if (currentSpeed >= 0f && moveHorizontal != 0f)
            {
                drifting = true;
                driftDirection = moveHorizontal > 0 ? 1 : -1;
            }

            canSteerWhenIdleDrifting = true;
            isStillJumpDrifting = true;
            camInstance.isCameraStill = true;
            sphereRigidBody.AddForce(kartModel.up * driftJumpForce, ForceMode.Force);

            //kartModel.parent.DOComplete();
            //kartModel.parent.DOPunchPosition(transform.up * .3f, driftDelayTime, 5, 1);
            waitForDriftingAgain = 0f;
        }
    }

    private void Boost(bool isDrifting = true)
    {
        if (!isOffroad)
        {
            if (isDrifting)
            {
                drifting = false;

                if (driftMode > 0)
                {
                    DOVirtual.Float(currentSpeed * boostMultiplier, currentSpeed, .3f * driftMode, Speed);
                    foreach(ParticleSystem boostParticle in boostParticles)
                    {
                        boostParticle.Play();
                    }
                }

                driftPower = 0;
                driftMode = 0;
                first = false; second = false; third = false;
            }
            else
            {
                if (steerMode > 0)
                {
                    DOVirtual.Float(currentSpeed * boostMultiplier, currentSpeed, .3f * steerMode, Speed);
                    foreach (ParticleSystem boostParticle in boostParticles)
                    {
                        boostParticle.Play();
                    }
                }
                steerMode = 0;
            }

            kartModel.parent.DOLocalRotate(Vector3.zero, .5f).SetEase(Ease.OutBack);
        }
        else
        {
            if (isDrifting)
            {
                drifting = false;
                driftPower = 0;
                driftMode = 0;
                first = false; second = false; third = false;
            }
            else
            {
                steerMode = 0;
            }
            kartModel.parent.DOLocalRotate(Vector3.zero, 1.5f).SetEase(Ease.OutBack);
        }
    }
    #endregion

    private void CheckDriftingForSkidMarks()
    {
        //if (drifting && currentSpeed > (0.4f * acceleration) && !isTouchingGrass && waitForDriftingAgain >= driftDelayTime)
        //{
        //    //StartSkidMarks();
        //}
        //else if (steerMode > 0 && currentSpeed > (0.4f * acceleration))
        //{
        //    //StartSkidMarks();
        //}
        //else
        //{
        //    //StopSkidMarks();
        //}
    }

    //private void StartSkidMarks()
    //{
    //    foreach (TrailRenderer tireSkidMark in tireSkidMarks)
    //    {
    //        tireSkidMark.emitting = true;
    //    }
    //}

    //private void StopSkidMarks()
    //{
    //    foreach (TrailRenderer tireSkidMark in tireSkidMarks)
    //    {
    //        tireSkidMark.emitting = false;
    //    }
    //}

    private void ColorDrift()
    {
        if (!first)
        {
            carStatus.color = Color.white;
        }

        if (driftPower > 50 && driftPower < 100 - 1 && !first)
        {
            first = true;
            driftMode = 1;
            carStatus.color = turboColors[0];
        }

        if (driftPower > 100 && driftPower < 150 - 1 && !second)
        {
            second = true;
            driftMode = 2;
            carStatus.color = turboColors[1];
        }

        if (driftPower > 150 && !third)
        {
            third = true;
            driftMode = 3;
            carStatus.color = turboColors[2];
        }
    }
    private void ColorSteer()
    {
        if (waitForSteerDrifting < 2f)
        {
            carStatus.color = Color.white;
            steerMode = 0;
        }
        else if (waitForSteerDrifting < 3.75f)
        {
            carStatus.color = turboColors[0];
            steerMode = 1;
        }
        else if (waitForSteerDrifting < 5.5f)
        {
            carStatus.color = turboColors[1];
            steerMode = 2;
        }
        else if (waitForSteerDrifting < 7.25f)
        {
            carStatus.color = turboColors[2];
            steerMode = 3;
        }
    }

    private void DisplayCarStatus()
    {
        displayCarStatus = !displayCarStatus;
        if (displayCarStatus == false && carStatusCanvas.activeSelf)
        {
            carStatusCanvas.SetActive(false);
        }

        if (displayCarStatus == true && !carStatusCanvas.activeSelf)
        {
            carStatusCanvas.SetActive(true);
        }
    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(sphereRigidBody.transform.position, Vector3.down*distanceToGround, Color.red);
        //Debug.DrawRay(transform.position, transform.forward * 20f, Color.blue);
        //Debug.DrawRay(transform.position, transform.right * 20f, Color.red);
    }

    #region ENVIRONMENT-STILL NOT IMPLEMENTED WELL ENOUGH
    public void EnviSpeedBooster()
    {
        speed = roadMaxSpeed;
        currentSpeed = speed;
        DOVirtual.Float(currentSpeed * enviHandler.speedBoosterMultiplier, currentSpeed, enviHandler.speedBoosterTime, Speed);
    }

    public void EnviCollideWithObstacle()
    {
        float bounceForce = realSpeed * enviHandler.bounceMultiplier;
        Debug.Log("Speed: " + realSpeed + " => " + bounceForce);

        if (bounceForce > enviHandler.minBounceForce)
        {
            bounceForce = Mathf.Clamp(currentSpeed * enviHandler.bounceMultiplier, enviHandler.minBounceForce, enviHandler.maxBounceForce);
            sphereRigidBody.AddForce(-transform.forward * bounceForce);
        }
        else
        {
            // stop rigidbody or something?
            isStuckAtObstacle = true;
        }
    }

    public void EnviExitObstacle()
    {
        isStuckAtObstacle = false;
    }
    #endregion
}
