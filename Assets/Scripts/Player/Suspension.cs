﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suspension : MonoBehaviour
{
    public List<GameObject> springs;
    public Rigidbody kartRigidBody;
    public float maxForce;
    public float maxDistance;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        foreach(GameObject spring in springs)
        {
            RaycastHit hit;
            if (Physics.Raycast(spring.transform.position, -transform.up, out hit, maxDistance))
            {
                kartRigidBody.AddForceAtPosition(maxForce * Time.fixedDeltaTime * transform.up * (maxDistance - hit.distance) / maxDistance, spring.transform.position);
            }
        }
    }
}
