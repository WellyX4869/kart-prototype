﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceController : MonoBehaviour
{
    public static SurfaceController instance;

    [Header("Physics Material")]
    [SerializeField] PhysicMaterial roadPhysics;
    [SerializeField] PhysicMaterial sandPhysics;
    [SerializeField] PhysicMaterial grassPhysics;

    [Header("Frictions Value (x = dynamic friction; y = static friction)")]
    public Vector2 defaultRoadFrictions;
    private Vector2 defaultSandFrictions;
    private Vector2 defaultGrassFrictions;
    public Vector2 roadFrictions;
    public Vector2 sandFrictions;
    public Vector2 grassFrictions;

    [Header("Other Special Frictions")]
    public Vector2 iceFrictions;
    public Vector2 pakemFrictions;

    [Header("Boolean Public Params")]
    public bool JalanNormal;
    public bool JalanPakemSekali;
    public bool JalanLicinSekali;

    public List<string> publicParams;

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;

        publicParams = new List<string>();
        publicParams.Add(nameof(JalanNormal));
        publicParams.Add(nameof(JalanPakemSekali));
        publicParams.Add(nameof(JalanLicinSekali));

        defaultRoadFrictions = new Vector2(roadPhysics.dynamicFriction, roadPhysics.staticFriction);
        defaultSandFrictions = new Vector2(sandPhysics.dynamicFriction, sandPhysics.staticFriction);
        defaultGrassFrictions = new Vector2(grassPhysics.dynamicFriction, grassPhysics.staticFriction);

        roadFrictions = new Vector2(defaultRoadFrictions.x, defaultRoadFrictions.y);
        sandFrictions = new Vector2(defaultSandFrictions.x, defaultSandFrictions.y);
        grassFrictions = new Vector2(defaultGrassFrictions.x, defaultGrassFrictions.y);

        JalanPakemSekali = false;
        JalanLicinSekali = false;
        JalanNormal = true;
    }

    public void ChangeSurfaceFrictions()
    {
        roadFrictions.x = defaultRoadFrictions.x; roadFrictions.y = defaultRoadFrictions.y;
        KartPrototypeController.instance.TimeToIdleSpeed = 0.5f;

        if (JalanPakemSekali)
        {
            roadFrictions.x = pakemFrictions.x; roadFrictions.y = pakemFrictions.y;
            KartPrototypeController.instance.TimeToIdleSpeed = 0.1f;
            JalanLicinSekali = false;
            JalanNormal = false;
        }
        
        if(JalanLicinSekali)
        {
            roadFrictions.x = iceFrictions.x; roadFrictions.y = iceFrictions.y;
            JalanPakemSekali = false;
            JalanNormal = false;
        }
        
        if (JalanNormal)
        {
            roadFrictions.x = defaultRoadFrictions.x; roadFrictions.y = defaultRoadFrictions.y;
            JalanLicinSekali = false;
            JalanPakemSekali = false;
        }
        roadPhysics.dynamicFriction = roadFrictions.x; roadPhysics.staticFriction = roadFrictions.y;
    }
}
